let a = 3;
let b = 10;

let resultadoResta = a - b;
let resultadoDivisión = a / b;
let resultadoMultiplicacion = a * b;
let resultadoEcuacion = (a - b) * a / b;

let resultadoSuma = a + b;                  // Suma numérica, resultado 13
let frase = "Hola me llamo" + " " + "Diego";  //Concatenación, el resultado sería la cadena 'Hola me llamo Edu'
let resultadoSuma2 = "3" + "10";            //Se trataría de una concatenación, el resultado sería 310
let resultadoSuma3 = "3" + 10;              //Javascript lo seguiría interpretando como cadena, resultado 310

console.log(frase)
console.log(resultadoSuma)
console.log(resultadoSuma2)
console.log(resultadoSuma3)
console.log(resultadoResta)
console.log(resultadoDivisión)
console.log(resultadoMultiplicacion)
console.log(resultadoEcuacion)

console.log("---------------------------------------------------------------------------------------------------------")

let tengoNetflix = true;
let tengoHBO = true;
let puedoVerSerie = tengoNetflix || tengoHBO;

let soyMayorDeEdad = true;
let tengoCarnet = true;
let puedoConducir = soyMayorDeEdad && tengoCarnet;

let noVerdadero = !true;
let noFalso = !false;

3 == 3      //True
3 == "3"    //True
3 == 4      //False
3 != 4      //True
3 != "3"    //False

3 === 3      //True
3 === "3"    //False
3 === 4      //False
3 !== 4      //True
3 !== "3"    //True

3 < 3      //False
3 <= 3     //True
3 > 4      //False
10 >= 4    //True