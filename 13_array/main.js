let numerosPremiados = [3, 5, 43, 18, 27];
let listaCompra = [ "Manzanas", "Peras", "Carne", "Papel de cocina"];
let varios = [3, "Manzanas", null, 18, true];

let familiaSimpson = [ "Homer", "Bart", "Lisa", "Marge", "Maggie" ];
let personaje = familiaSimpson[0];
console.log(personaje); //Mostrará por consola a Homer

for ( let i = 0; i < familiaSimpson.length; i++){
    console.log(familiaSimpson[i]);
}

familiaSimpson[1] = "Mou"; //Estamos cambiando a Bart por Mou
console.log(familiaSimpson);

familiaSimpson.pop();
console.log(familiaSimpson); //Mostrará el listado sin Maggie

familiaSimpson.pop();
console.log(familiaSimpson); //Mostrará el listado sin Maggie y sin Marge

familiaSimpson.push("Bola de Nieve");
console.log(familiaSimpson); //Mostrará el listado añadiendo al final a Bola de Nieve

familiaSimpson.push("Ayudante de Santa Claus", "Abe");
console.log(familiaSimpson); //Mostrará el listado añadiendo al final a Ayudante de Santa Claus y a Abe