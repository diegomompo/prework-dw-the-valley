let zapatilla = {
    marca: "Nike",
    modelo: "Air Max",
    talla: 45,
    precio: 100
};

let marcaZapatilla = zapatilla["marca"];
// let marcaZapatilla = zapatilla.marca también valdría

console.log(marcaZapatilla);

zapatilla["talla"] = 38;

console.log(zapatilla["talla"]);

console.log(zapatilla["color"]); // Nos devolverá undefined

zapatilla["color"] = "Blanco";
//zapatilla.color = "Blanco" también valdría

console.log(zapatilla); // Ya tendríamos la propiedad incluida en el objeto