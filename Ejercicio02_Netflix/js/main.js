var disponible = true;

// FUNCIONES DEL HTML

window.onload = function(){
    $(document).ready(bloqueIntro);
    $(document).ready(bloqueBanner);
    $(document).ready(bloqueFAQ);
    $(document).ready(bloqueFooter);
}

function bloqueIntro(){

    var sectionIntro = $("<section>");
    sectionIntro.attr("class", "intro");

    var headerIntro = $("<header>");

    var imgLogo = $("<img>");
    imgLogo.attr({src:"assets/logo.png", class:"logo"});

    var buttonIntro = $("<button>");
    buttonIntro.attr({class:"login-button"});
    buttonIntro.text("Iniciar sesión");

    var h1Intro = $("<h1>");
    h1Intro.attr({class:"intro-title"})
    h1Intro.text("Todas las películas y series que desees, y mucho más.")

    var h2Intro = $("<h2>");
    h2Intro.attr({class:"intro-subtitle"})
    h2Intro.text("Disfruta donde quieras. Cancela cuando quieras.")

    var pIntro = $("<p>");
    pIntro.attr({class:"subscribe-intro-text"})
    pIntro.text("¿Quieres ver algo ya? Escribe tu dirección de correo para crear una suscripción a Netflix o reactivarla.")

    var formIntro = $("<form>");
    formIntro.attr({class:"subscribe-form"})

    var inputIntro = $("<input>");
    inputIntro.attr({class:"subscribe-input", type:"text",placeholder:"Dirección de correo"});

    var inputButtonIntro = $("<input>");
    inputButtonIntro.attr({class:"subscribe-button", type:"button", value:"Empezar"});


    $("body").append(sectionIntro);
    $(sectionIntro).append(headerIntro);
    $(headerIntro).append(imgLogo);
    $(headerIntro).append(buttonIntro);
    $(sectionIntro).append(h1Intro);
    $(sectionIntro).append(h2Intro);
    $(sectionIntro).append(pIntro);
    $(sectionIntro).append(formIntro);
    $(formIntro).append(inputIntro);
    $(formIntro).append(inputButtonIntro);
}

function bloqueBanner(){
    var sectionBanner = $("<section>");
    sectionBanner.attr("class", "banner");
    var divBanner = $("<div>")
    divBanner.attr({class: "banner-info"})
    var h3Banner = $("<h3>");
    h3Banner.attr({class:"banner-title"})
    h3Banner.text("Disfruta de Netflix en tu TV.")
    var pBanner= $("<p>");
    pBanner.attr({class:"banner-text"})
    pBanner.text("Smart TV, Playstation, Xbox, Chromecast, Apple TV, reproductores Blu-ray y muchos más")
    var imgBanner = $("<img>");
    imgBanner.attr({src:"assets/banner-1.png", class:"banner-image"});

    var sectionBanner2 = $("<section>");
    sectionBanner2.attr("class", "banner");
    var divBanner2 = $("<div>")
    divBanner2.attr({class: "banner-info"})
    var h3Banner2 = $("<h3>");
    h3Banner2.attr({class:"banner-title2"})
    h3Banner2.text("Descárgate tus series favoritas para verlas sin conexión.")
    var pBanner2= $("<p>");
    pBanner2.attr({class:"banner-text2"})
    pBanner2.text("Guarda tus títulos favoritos fácilmente para que siempre tengas algo para ver")
    var imgBanner2 = $("<img>");
    imgBanner2.attr({src:"assets/banner-2.png", class:"banner-image2"});

    var sectionBanner3 = $("<section>");
    sectionBanner3.attr("class", "banner");
    var divBanner3 = $("<div>")
    divBanner3.attr({class: "banner-info"})
    var h3Banner3 = $("<h3>");
    h3Banner3.attr({class:"banner-title"})
    h3Banner3.text("Disfruta en todas partes.")
    var pBanner3= $("<p>");
    pBanner3.attr({class:"banner-text"})
    pBanner3.text("Reproduce en streaming todas las películas y series en tu móvil, tableta, ordenador y TV sin pagar más.")
    var imgBanner3 = $("<img>");
    imgBanner3.attr({src:"assets/banner-3.png", class:"banner-image"});

    var sectionBanner4 = $("<section>");
    sectionBanner4.attr("class", "banner");
    var divBanner4 = $("<div>")
    divBanner4.attr({class: "banner-info"})
    var h3Banner4 = $("<h3>");
    h3Banner4.attr({class:"banner-title2"})
    h3Banner4.text("Crea perfiles infantiles.")
    var pBanner4= $("<p>");
    pBanner4.attr({class:"banner-text2"})
    pBanner4.text("Deja que los niños vivan aventuras con sus personahjes en un espacio diseñado exclusivamente para ellos, gratis con su suscripción")
    var imgBanner4 = $("<img>");
    imgBanner4.attr({src:"assets/banner-4.png", class:"banner-image2"});

    $("body").append(sectionBanner)
    $(sectionBanner).append(divBanner)
    $(divBanner).append(h3Banner)
    $(divBanner).append(pBanner)
    $(sectionBanner).append(imgBanner)

    $("body").append(sectionBanner2)
    $(sectionBanner2).append(divBanner2)
    $(divBanner2).append(h3Banner2)
    $(divBanner2).append(pBanner2)
    $(sectionBanner2).append(imgBanner2)

    $("body").append(sectionBanner3)
    $(sectionBanner3).append(divBanner3)
    $(divBanner3).append(h3Banner3)
    $(divBanner3).append(pBanner3)
    $(sectionBanner3).append(imgBanner3)

    $("body").append(sectionBanner4)
    $(sectionBanner4).append(divBanner4)
    $(divBanner4).append(h3Banner4)
    $(divBanner4).append(pBanner4)
    $(sectionBanner4).append(imgBanner4)
}

function bloqueFAQ(){
    var sectionFAQ = $("<section>");
    sectionFAQ.attr("class", "faqs");

    var h2Banner = $("<h2>");
    h2Banner.attr({class:"faqs-title"})
    h2Banner.text("Preguntas frecuentes")

    var details1FAQ = $("<details>")
    details1FAQ.attr({class: "faq"})
    var summary1Banner = $("<summary>")
    summary1Banner.text("¿Qué es Netflix?")
    var p1Banner = $("<p>")
    p1Banner.text("Netflix es un servicio de streaming que ofrece una amplia variedad de series, películas, títulos de anime, documentales y otros contenidos premiados en miles de dispositivos conectados a internet.")
    var p11Banner = $("<p>")
    p11Banner.text("Disfruta de Netflix en tu smartphone, tableta, Smart TV, ordenador o dispositivo de streaming, todo por una tarifa mensual fija. Planes desde 7,99 € a 17,99 € al mes. Sin cargos adicionales ni contratos.")

    var details2FAQ = $("<details>")
    details2FAQ.attr({class: "faq"})
    var summary2Banner = $("<summary>")
    summary2Banner.text("¿Cuánto cuesta Netflix?")
    var p2Banner = $("<p>")
    p2Banner.text("Disfruta de Netflix en tu smartphone, tableta, Smart TV, ordenador o dispositivo de streaming, todo por una tarifa mensual fija. Planes desde 7,99 € a 17,99 € al mes. Sin cargos adicionales ni contratos.")

    var details3FAQ = $("<details>")
    details3FAQ.attr({class: "faq"})
    var summary3Banner = $("<summary>")
    summary3Banner.text("¿Dónde puedo ver Netflix?")
    var p3Banner = $("<p>")
    p3Banner.text("Disfruta donde quieras y cuando quieras. Inicia sesión con tu cuenta de Netflix para disfrutar al instante de todo el contenido de netflix.com desde tu ordenador personal o en cualquier dispositivo conectado a internet que ofrezca la aplicación de Netflix, entre ellos, smart TV, smartphones, tabletas, reproductores multimedia de streaming y consolas de juegos.")

    var details4FAQ = $("<details>")
    details4FAQ.attr({class: "faq"})
    var summary4Banner = $("<summary>")
    summary4Banner.text("¿Cómo cancelo?")
    var p4Banner = $("<p>")
    p4Banner.text("Netflix es flexible. Sin contratos liosos ni compromisos. Puedes cancelar fácilmente tu cuenta en línea con tan solo dos clics. Sin cargos por cancelación: activa o cancela tu cuenta en cualquier momento.")

    var details5FAQ = $("<details>")
    details5FAQ.attr({class: "faq"})
    var summary5Banner = $("<summary>")
    summary5Banner.text("¿Qué puedo ver en Netflix?")
    var p5Banner = $("<p>")
    p5Banner.text("Netflix dispone de una amplia biblioteca de originales de Netflix galardonados, títulos de anime, series de TV, documentales, largometrajes y otros contenidos. Ve todo el contenido que quieras, cuando quieras.")

    var details6FAQ = $("<details>")
    details6FAQ.attr({class: "faq"})
    var summary6Banner = $("<summary>")
    summary6Banner.text("¿Es Netflix bueno para los niños?")
    var p6Banner = $("<p>")
    p6Banner.text("La experiencia infantil de Netflix se incluye en la suscripción para que los padres tengan el control mientras los niños disfrutan de series y películas familiares en su propio espacio.")

    var pFAQ = $("<p>");
    pFAQ.attr({class:"subscribe-faq-text"})
    pFAQ.text("¿Quieres ver algo ya? Escribe tu dirección de correo para crear una suscripción a Netflix o reactivarla.")

    var formFAQ = $("<form>");
    formFAQ.attr({class:"subscribe-form"})

    var inputFAQ = $("<input>");
    inputFAQ.attr({class:"subscribe-input", type:"text",placeholder:"Dirección de correo"});

    var inputButtonFAQ = $("<input>");
    inputButtonFAQ.attr({class:"subscribe-button", type:"button", value:"Empezar"});
    
    $("body").append(sectionFAQ)
    $(sectionFAQ).append(h2Banner)

    $(sectionFAQ).append(details1FAQ)
    $(details1FAQ).append(summary1Banner)
    $(details1FAQ).append(p1Banner)
    $(details1FAQ).append(p11Banner)

    $(sectionFAQ).append(details2FAQ)
    $(details2FAQ).append(summary2Banner)
    $(details2FAQ).append(p2Banner)

    $(sectionFAQ).append(details3FAQ)
    $(details3FAQ).append(summary3Banner)
    $(details3FAQ).append(p3Banner)

    $(sectionFAQ).append(details4FAQ)
    $(details4FAQ).append(summary4Banner)
    $(details4FAQ).append(p4Banner)

    $(sectionFAQ).append(details5FAQ)
    $(details5FAQ).append(summary5Banner)
    $(details5FAQ).append(p5Banner)

    $(sectionFAQ).append(details6FAQ)
    $(details6FAQ).append(summary6Banner)
    $(details6FAQ).append(p6Banner)

    $(sectionFAQ).append(pFAQ);
    $(sectionFAQ).append(formFAQ);
    $(formFAQ).append(inputFAQ);
    $(formFAQ).append(inputButtonFAQ);
}

function bloqueFooter(){

    var footer = $("<footers>")
    footer.attr({class : "footer"})

    var pFooter = $("<p>");
    pFooter.attr({class:"footer-message"})
    pFooter.text("Preguntas? Llama al 900 822 376")

    var divFotter = $("<div>")
    divFotter.attr({class: "footer-links"})

    var divColumn1Footers = $("<div>")
    divColumn1Footers.attr({class: "footer-links-column"})
    var aColumn1Footers1 = $("<a>")
    aColumn1Footers1.attr({class: "footer-link", href:"#"})
    aColumn1Footers1.text("Preguntas Frecuentes")
    var aColumn1Footers2 = $("<a>")
    aColumn1Footers2.attr({class: "footer-link", href:"#"})
    aColumn1Footers2.text("Inversores")
    var aColumn1Footers3 = $("<a>")
    aColumn1Footers3.attr({class: "footer-link", href:"#"})
    aColumn1Footers3.text("Formas de ver")
    var aColumn1Footers4 = $("<a>")
    aColumn1Footers4.attr({class: "footer-link", href:"#"})
    aColumn1Footers4.text("Información corporativa")
    var aColumn1Footers5 = $("<a>")
    aColumn1Footers5.attr({class: "footer-link", href:"#"})
    aColumn1Footers5.text("Avisos legales")

    var divColumn2Footers = $("<div>")
    divColumn2Footers.attr({class: "footer-links-column"})
    var aColumn2Footers1 = $("<a>")
    aColumn2Footers1.attr({class: "footer-link", href:"#"})
    aColumn2Footers1.text("Centro de ayuda")
    var aColumn2Footers2 = $("<a>")
    aColumn2Footers2.attr({class: "footer-link", href:"#"})
    aColumn2Footers2.text("Empleo")
    var aColumn2Footers3 = $("<a>")
    aColumn2Footers3.attr({class: "footer-link", href:"#"})
    aColumn2Footers3.text("Términos de uso")
    var aColumn2Footers4 = $("<a>")
    aColumn2Footers4.attr({class: "footer-link", href:"#"})
    aColumn2Footers4.text("Contáctanos")
    var aColumn2Footers5 = $("<a>")
    aColumn2Footers5.attr({class: "footer-link", href:"#"})
    aColumn2Footers5.text("Solo en Netflix")

    var divColumn3Footers = $("<div>")
    divColumn3Footers.attr({class: "footer-links-column"})
    var aColumn3Footers1 = $("<a>")
    aColumn3Footers1.attr({class: "footer-link", href:"#"})
    aColumn3Footers1.text("Cuenta")
    var aColumn3Footers2 = $("<a>")
    aColumn3Footers2.attr({class: "footer-link", href:"#"})
    aColumn3Footers2.text("Canjear Tarjetas Regalo")
    var aColumn3Footers3 = $("<a>")
    aColumn3Footers3.attr({class: "footer-link", href:"#"})
    aColumn3Footers3.text("Privacidad")
    var aColumn3Footers4 = $("<a>")
    aColumn3Footers4.attr({class: "footer-link", href:"#"})
    aColumn3Footers4.text("Prueba de velocidad")

    var divColumn4Footers = $("<div>")
    divColumn4Footers.attr({class: "footer-links-column"})
    var aColumn4Footers1 = $("<a>")
    aColumn4Footers1.attr({class: "footer-link", href:"#"})
    aColumn4Footers1.text("Zona de prensa")
    var aColumn4Footers2 = $("<a>")
    aColumn4Footers2.attr({class: "footer-link", href:"#"})
    aColumn4Footers2.text("Canjear Tarjetas Regalo")
    var aColumn4Footers3 = $("<a>")
    aColumn4Footers3.attr({class: "footer-link", href:"#"})
    aColumn4Footers3.text("Preferencias de cookies")
    var aColumn4Footers4 = $("<a>")
    aColumn4Footers4.attr({class: "footer-link", href:"#"})
    aColumn4Footers4.text("Garantía legal")

    $("body").append(footer)
    $(footer).append(pFooter)
    $(footer).append(divFotter)

    $(divFotter).append(divColumn1Footers)
    $(divColumn1Footers).append(aColumn1Footers1)
    $(divColumn1Footers).append(aColumn1Footers2)
    $(divColumn1Footers).append(aColumn1Footers3)
    $(divColumn1Footers).append(aColumn1Footers4)
    $(divColumn1Footers).append(aColumn1Footers5)

    $(divFotter).append(divColumn2Footers)
    $(divColumn2Footers).append(aColumn2Footers1)
    $(divColumn2Footers).append(aColumn2Footers2)
    $(divColumn2Footers).append(aColumn2Footers3)
    $(divColumn2Footers).append(aColumn2Footers4)
    $(divColumn2Footers).append(aColumn2Footers5)

    $(divFotter).append(divColumn3Footers)
    $(divColumn3Footers).append(aColumn3Footers1)
    $(divColumn3Footers).append(aColumn3Footers2)
    $(divColumn3Footers).append(aColumn3Footers3)
    $(divColumn3Footers).append(aColumn3Footers4)

    $(divFotter).append(divColumn4Footers)
    $(divColumn4Footers).append(aColumn4Footers1)
    $(divColumn4Footers).append(aColumn4Footers2)
    $(divColumn4Footers).append(aColumn4Footers3)
    $(divColumn4Footers).append(aColumn4Footers4)





}
