console.log("EJERCICIO 1")

// Cuenta del 330 al 910

for (let i = 330; i <= 910; i++) {
   console.log(i);
}

console.log("EJERCICIO 2")

let sumador = 0;

for (let i = 1; i < 1000; i++){
    if( i % 2 === 0) {   
        console.log(i);
        sumador += i;
    }
}

console.log(sumador);

console.log("EJERCICIO 3")

// Cuenta el número de 'i' que tiene la palabra 'supercalifragilisticoespialidoso’

let palabra = "supercalifragilisticoespialidoso";
let contador = 0;

for (let i=0; i < palabra.length; i++){
    if (palabra[i] === 'i'){
        contador++;
    }
}

console.log(contador);