console.log("EJERCICIO 1")


// A
let persona = {
    nombre: "Eduardo",
    apellido: "Cuadrado",
    edad: 31,
    sexo: "Hombre"
};

console.log(persona);

// B
persona.nombre = "Pepito";
persona.apellido = "García";
persona.edad = 45;
persona.sexo = "Hombre";

console.log(persona);


console.log("EJERCICIO 2")

let coche = {
  marca: "Fiat",
  modelo: "500",
  ano: 2020
};

console.log(coche);

// B y C
if ( (2022 - coche.ano) > 25 ) {
  console.log("El " + coche.marca + " " + coche.modelo + " es un clásico de " + coche.ano);
} else {
  console.log("El " + coche.marca + " " + coche.modelo + " del " + coche.ano + " no es un clásico");
} 

console.log("EJERCICIO 3")

let propiedades = ["marca", "modelo", "ano"];
let valores = ["Fiat", "500", 2020];
let coche2 = new Object();

for (let i=0; i < propiedades.length; i++){
  coche2[propiedades[i]] = valores[i];
}

console.log(coche2);