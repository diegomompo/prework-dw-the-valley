console.log("EJERCICIO 1")


function calcularPrecioDescuento(precio, descuento) {
  return precio - precio * ( descuento / 100 );
}

let precioFinal=calcularPrecioDescuento(120, 30);
console.log(precioFinal);


console.log("EJERCICIO 2")

let listaCompra = ['carne', 'pescado', 'jabón'];

function anadirElemento(lista, alimento) {
    lista.push(alimento);
}

anadirElemento(listaCompra, 'fruta');
console.log(listaCompra);

function eliminarElemento(lista) {
    lista.pop();
}

eliminarElemento(listaCompra);
console.log(listaCompra);


console.log("EJERCICIO 3")

let usuario = {
  nombre: "Fernando",
  apellido: "García",
  email: "fgarcia@mail.com",
  compras: [100, 20, 100, 50, 300],
  tipo: "NORMAL"
};

function actualizarTipoCliente (cliente) {
let suma = sumadorCompras(cliente.compras); 
if(suma > 500 ){
  cliente.tipo = "VIP";
} else { 
  cliente.tipo = "NORMAL";
}
}

function sumadorCompras(compras){
let sumaCompras = 0;
for( let i= 0; i < compras.length; i++){
  sumaCompras += compras[i];
}
return sumaCompras;
}

actualizarTipoCliente (usuario);
console.log(usuario);