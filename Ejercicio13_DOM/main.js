console.log("EJERCICIO 1")


let elementoH2 = document.querySelector('h2');
elementoH2.textContent = 'Hola Don José';


console.log("EJERCICIO 2")

let listado = ['Homer','Lisa','Marge','Bart'];

let elementoContenedor = document.getElementById('contenedor');
let elementoLista = document.createElement('ul');

for ( let i = 0; i < listado.length; i++){
    let elementoItem = document.createElement('li');
    elementoItem.textContent = listado[i];

    elementoLista.appendChild(elementoItem);
}

elementoContenedor.appendChild(elementoLista);

console.log("EJERCICIO 3")

let listado2 = ['VERDE','AMARILLO','AZUL','MARRÓN','ROJO','NARANJA'];

let elementoContenedor2 = document.getElementById('contenedor');
let elementoLista1 = document.createElement('ul');
let elementoLista2 = document.createElement('ul');

for ( let i = 0; i < listado2.length; i++){
    let elementoItem = document.createElement('li');
    elementoItem.textContent = listado2[i];
    if(listado2[i].length > 4){
        elementoLista1.appendChild(elementoItem);
    } else{
        elementoLista2.appendChild(elementoItem);
    }
}

elementoContenedor2.appendChild(elementoLista1);
elementoContenedor2.appendChild(elementoLista2);

console.log("EJERCICIO 4")

let elementosH1 = document.querySelectorAll('h1');
let elementosH2 = document.querySelectorAll('h2');

function changeColor(elementos, color) {
    for (let i = 0; i < elementos.length; i++){
        elementos[i].style.color = color;
    }
}

changeColor(elementosH1,'red');
changeColor(elementosH2,'green');
