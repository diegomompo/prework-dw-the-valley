console.log("EJERCICIO 1")


let elementoBtn= document.querySelector('button'); 
elementoBtn.addEventListener('click', mostrarGIF);

function mostrarGIF (){
    let elementoImg = document.createElement('img');
    elementoImg.setAttribute('src','./imagen.gif');

    let elementoBody = document.querySelector('body');
    elementoBody.textContent = '';
    elementoBody.appendChild(elementoImg);
}

console.log("EJERCICIO 2")

let numero = 100;

elementoBtnSuma = document.getElementById('btnSuma'); 
elementoBtnResta = document.getElementById('btnResta'); 
elementoP = document.getElementById('numero'); 

elementoBtnSuma.addEventListener('click', function () { operacion('suma')});
elementoBtnResta.addEventListener('click', function () { operacion('resta')});

function operacion (valor) {
    if(valor == 'suma'){
        numero += 1;
    } else {
        numero -= 1;
    }
    elementoP.textContent = numero;
}


console.log("EJERCICIO 3")

let colores = ['green', 'red', 'blue', 'yellow'];

let elementoH2= document.querySelector('h2'); 
let elementoBody = document.querySelector('body');

function crearBotones (colores){
    for (let i = 0; i < colores.length; i++) {
        let elementoBtn = document.createElement('button');
        elementoBtn.textContent = colores[i];
        elementoBtn. addEventListener('click', function() { pintarTexto(colores[i])});
        elementoBody.appendChild(elementoBtn);
    }
}

function pintarTexto(color){
    elementoH2.style.color = color;
}

crearBotones(colores);
