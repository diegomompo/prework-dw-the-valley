let productos = [
    {
        id: 1,
        nombre: 'Big Mac',
        precio: 8.15,
        imagen: 'item1.png'
    },
    {
        id: 2,
        nombre: 'American Chicken',
        precio: 9.35,
        imagen: 'item2.png'
    },
    {
        id: 3,
        nombre: 'Grand McExtreme',
        precio: 10.45,
        imagen: 'item3.png'
    },
    {
        id: 4,
        nombre: 'McMenu CBO',
        precio: 9.55,
        imagen: 'item4.png'
    },
    {
        id: 5,
        nombre: 'McNuggets',
        precio: 10.95,
        imagen: 'item5.png'
    },
    {
        id: 5,
        nombre: 'Alitas/McNuggets',
        precio: 11.25,
        imagen: 'item6.png'
    },
    {
        id: 6,
        nombre: 'McFlurry Oreo',
        precio: 33.55,
        imagen: 'item6.png'
    }


];

let cartItems = [];

function renderProducts(){

    for( let i = 0; i < productos.length; i++){

        let divElementItem = $("<div>");
        divElementItem.attr({class: "item"});

        let divElementItemDescription = $("<div>");
        divElementItemDescription.attr({class : "item-description"});

        let imgElementItem = $("<img>");
        let url = './assets/' + productos[i].imagen;
        imgElementItem.attr({class: "item-img", src :url});

        let nameElementItem = $("<p>");
        nameElementItem.attr({class: "item-name"});
        nameElementItem.text(productos[i].nombre);

        let divElementItemOrder = $("<div>");
        divElementItemOrder.attr({class : "item-order"});

        let priceElementItem = $("<p>");
        priceElementItem.attr({class: "item-price"});
        priceElementItem.text(productos[i].precio + '€');

        let buttonElementItem = $("<button>");
        buttonElementItem.attr({class:"item-add"});
        buttonElementItem.text("+");
        buttonElementItem.click(function() { addProductsToCart(productos[i]) });

        divElementItemDescription.append(imgElementItem);
        divElementItemDescription.append(nameElementItem);

        divElementItemOrder.append(priceElementItem);
        divElementItemOrder.append(buttonElementItem);

        divElementItem.append(divElementItemDescription);
        divElementItem.append(divElementItemOrder);

        $("#listItems").append(divElementItem);
    }
}

function addProductsToCart(item){
    cartItems.push(item);
    renderCart();
}
function renderCart(){

    let elementCartList = document.getElementById('cartItems');
    $("#cartItems").text(""); /* Truki para eliminar contenido dentro del div */

    let totalPrice = 0;
    //comprobamos el estado del carrito, si no tiene items pintamos el dibujo de carrito vacío
    if(cartItems.length > 0){
        //eliminaremos la información de que no hay nada en el carro
        $("#cartNoItems").hide();

        //pintaremos la lista
        for( let i = 0; i < cartItems.length; i++){
            let elementCartItem = $("<div>");
            elementCartItem.attr({class : "cart-item"});

            let elementCartQuantity = $("<p>");
            elementCartQuantity.attr({class: "cart-item-quantity"});
            elementCartQuantity.text("1X");

            let elementCartDescription = $("<p>");
            elementCartDescription.attr({class: "cart-item-description"});
            elementCartDescription.text(cartItems[i].nombre);

            let elementCartPrice = $("<p>");
            elementCartPrice.attr({class:"cart-item-price"});
            elementCartPrice.text(cartItems[i].precio + '€');

            let elementItemBtn = $("<button>");
            elementItemBtn.attr({class:"cart-item-btn"});
            elementItemBtn.text('-');
            elementItemBtn.click(function() { deleteProductFromCart(i) });
            
            elementCartItem.append(elementCartQuantity);
            elementCartItem.append(elementCartDescription);
            elementCartItem.append(elementCartPrice);
            elementCartItem.append(elementItemBtn);

            $("#cartItems").append(elementCartItem);
            console.log(cartItems[i].precio);
            totalPrice += cartItems[i].precio;
            console.log(totalPrice)            
        }

        let elementTotal = $("<p>");
        elementTotal.attr({class: "cart-total"});
        elementTotal.text("Total: " + totalPrice + "€");

        $("#cartItems").append(elementTotal);

    } else {
        //mostraremos información de que no hay nada en el carro
        $("#cartNoItems").show();
    }
}

function deleteProductFromCart(elementPosition){
    let cartItemsAux = [];

    for(let i = 0; i< cartItems.length; i++){
        if(i !== elementPosition){
            cartItemsAux.push(cartItems[i]);
        }
    }
    cartItems = cartItemsAux;
    renderCart();
}