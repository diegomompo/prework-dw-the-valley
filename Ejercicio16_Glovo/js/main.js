var disponible = true

//FUNCIONES HTML

window.onload = function () {
    $(document).ready(bloqueHeader)
    $(document).ready(bloqueSection)
    $(document).ready(renderProducts)
    $(document).ready(renderCart)
    $(document).ready(bloqueFooter)
}

function bloqueHeader(){
    //VARIABLES

    let header = $("<header>")
    let imgHeader = $("<img>")

    //ATRIBUTOS

    header.attr({class: "header"})
    imgHeader.attr({class: "logo", src:"assets/logo.png"})

    $("body").append(header)
    $(header).append(imgHeader)
}
function bloqueFooter(){
    
    //VARIBALES DEL BLOQUE

    var footer = $("<footer>")
    var divFooterContainer = $("<div>")
    let imgFooter = $("<img>")
    var divFooter = $("<div>")
    var divColumnFooters1 = $("<div>")
    var pColumn1Footers1 = $("<p>")
    var aColumn1Footers1 = $("<a>")
    var aColumn1Footers2 = $("<a>")
    var aColumn1Footers3 = $("<a>")
    var aColumn1Footers4 = $("<a>")
    var divColumnFooters2 = $("<div>")
    var pColumn2Footers1 = $("<p>")
    var aColumn2Footers1 = $("<a>")
    var aColumn2Footers2 = $("<a>")
    var aColumn2Footers3 = $("<a>")
    var aColumn2Footers4 = $("<a>")
    var aColumn2Footers5 = $("<a>")
    var divColumnFooters3 = $("<div>")
    var pColumn3Footers1 = $("<p>")
    var aColumn3Footers1 = $("<a>")
    var aColumn3Footers2 = $("<a>")
    var aColumn3Footers3 = $("<a>")
    var aColumn3Footers4 = $("<a>")

    //ATRIBUTOS BLOQUE

    footer.attr({class : "footer"})
    divFooterContainer.attr({class: "footer-container"})
    imgFooter.attr({class: "logo", src:"assets/logo-footer.png"})
    divFooter.attr({class: "footer-links"})
    divColumnFooters1.attr({class: "footer-links-column", href:"#"})
    pColumn1Footers1.attr({class: "footer-link-title"})
    aColumn1Footers1.attr({class: "footer-link", href:"#"})
    aColumn1Footers2.attr({class: "footer-link", href:"#"})
    aColumn1Footers3.attr({class: "footer-link", href:"#"})
    aColumn1Footers4.attr({class: "footer-link", href:"#"})
    divColumnFooters2.attr({class: "footer-links-column", href:"#"})
    pColumn2Footers1.attr({class: "footer-link-title"})
    aColumn2Footers1.attr({class: "footer-link", href:"#"})
    aColumn2Footers2.attr({class: "footer-link", href:"#"})
    aColumn2Footers3.attr({class: "footer-link", href:"#"})
    aColumn2Footers4.attr({class: "footer-link", href:"#"})
    aColumn2Footers5.attr({class: "footer-link", href:"#"})
    divColumnFooters3.attr({class: "footer-links-column", href:"#"})
    pColumn3Footers1.attr({class: "footer-link-title"})
    aColumn3Footers1.attr({class: "footer-link", href:"#"})
    aColumn3Footers2.attr({class: "footer-link", href:"#"})
    aColumn3Footers3.attr({class: "footer-link", href:"#"})
    aColumn3Footers4.attr({class: "footer-link", href:"#"})

    //TEXTOS DEL BLOQUE

    pColumn1Footers1.text("Colabora con Glovo")
    aColumn1Footers1.text("Trabaja con nosotros")
    aColumn1Footers2.text("Glovo para Partners")
    aColumn1Footers3.text("Repartidores")
    aColumn1Footers4.text("Glovo Business")

    pColumn2Footers1.text("Enlance de interés")
    aColumn2Footers1.text("Acerca de nosotros")
    aColumn2Footers2.text("Preguntas frecuentes")
    aColumn2Footers3.text("Blog")
    aColumn2Footers4.text("Contacto")
    aColumn2Footers5.text("Seguridad")

    pColumn2Footers1.text("Sígenos")
    aColumn2Footers1.text("Facebook")
    aColumn2Footers2.text("Twitter")
    aColumn2Footers3.text("Instagram")

    //MOSTRAR EN PANTALLA

    $("body").append(footer)
    $(footer).append(divFooterContainer)

    $(divFooterContainer).append(imgFooter)

    $(divFooterContainer).append(divFooter)

    $(divFooter).append(divColumnFooters1)
    $(divColumnFooters1).append(pColumn1Footers1)
    $(divColumnFooters1).append(aColumn1Footers1)
    $(divColumnFooters1).append(aColumn1Footers2)
    $(divColumnFooters1).append(aColumn1Footers3)
    $(divColumnFooters1).append(aColumn1Footers4)

    $(divFooter).append(divColumnFooters2)
    $(divColumnFooters2).append(pColumn2Footers1)
    $(divColumnFooters2).append(aColumn2Footers1)
    $(divColumnFooters2).append(aColumn2Footers2)
    $(divColumnFooters2).append(aColumn2Footers3)
    $(divColumnFooters2).append(aColumn2Footers4)
    $(divColumnFooters2).append(aColumn2Footers5)

    $(divFooter).append(divColumnFooters3)
    $(divColumnFooters3).append(pColumn3Footers1)
    $(divColumnFooters3).append(aColumn3Footers1)
    $(divColumnFooters3).append(aColumn3Footers2)
    $(divColumnFooters3).append(aColumn3Footers3)
}