var section = $("<section>")

let divSection = $("<div>")
divSection.attr({id: "listItems", class: "restaurant-items"})


let divNoProducts = $("<div>")
let divProducts = $("<div>")
divNoProducts.attr({id:"cartNoItems", class: "cart-no-products"})
divProducts.attr({id:"cartItems", class:"cart-products"})

function bloqueSection(){
    //VARIABLE

    bloqueSectionProducts()
    bloqueSectionAside()

    $("body").append(section)

}

function bloqueSectionProducts(){
    //VARIABLE
    let mainSection = $("<main>")
    let h2Section = $("<h2>")
    let pSection = $("<p>")
    let h3Section = $("<h3>")

    //ATRIBUTOS
    mainSection.attr({class: "main custom-border"})
    h2Section.attr({class: "restaurant-title"})
    pSection.attr({class: "restaurant-info"})
    h3Section.attr({class: "restaurant-items-title"})

    //TEXTO
    h2Section.text("Mcdonald's")
    pSection.text("Tiempo de entrega: 15-20")
    h3Section.text("Top ventas")


    $(section).append(mainSection)
    $(mainSection).append(h2Section)
    $(mainSection).append(pSection)
    $(mainSection).append(h3Section)
    $(mainSection).append(divSection)
}

function bloqueSectionAside(){
    //VARIABLES
    let asideSection = $("<aside>")
    let pCart = $("<p>")

    let imgNoProducts = $("<img>")
    let pNoProducts = $("<p>")




    //ATRIBUTOS
    asideSection.attr({class: "cart custom-border"})
    pCart.attr({class: "cart-title"})

    imgNoProducts.attr({class: "no-products-img", src:"assets/sin-productos.png"})
    pNoProducts.attr({class: "no-product-text"})


    //TEXTO

    pCart.text("Tu Glovo")
    pNoProducts.text("Todavía no has añadido ningún producto. Cuando lo hagas, ¡verás los productos aquí!")


    $(section).append(asideSection)
    $(asideSection).append(pCart)

    $(asideSection).append(divNoProducts)
    $(divNoProducts).append(imgNoProducts)
    $(divNoProducts).append(pNoProducts)

    $(asideSection).append(divProducts)

}