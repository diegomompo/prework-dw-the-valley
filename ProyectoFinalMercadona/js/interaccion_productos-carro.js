/*
    Base de datos de productos
*/

let productos = [
    {
        id: 1,
        nombre: 'Pera Conferencia',
        precio: 0.34,
        imagen: 'item1.jpeg'
    },
    {
        id: 2,
        nombre: 'Manzana Golden',
        precio: 0.36,
        imagen: 'item2.jpeg'
    },
    {
        id: 3,
        nombre: 'Uvas',
        precio: 2.60,
        imagen: 'item3.jpeg'
    },
    {
        id: 4,
        nombre: 'Banana',
        precio: 0.21,
        imagen: 'item4.jpeg'
    },
    {
        id: 5,
        nombre: 'Melón Galia',
        precio: 2.71,
        imagen: 'item5.jpeg'
    },
    {
        id: 6,
        nombre: 'Mandarina',
        precio: 0.36,
        imagen: 'item6.jpeg'
    },
    {
        id: 7,
        nombre: 'Piña',
        precio: 2.30,
        imagen: 'item7.jpeg'
    },
    {
        id: 8,
        nombre: 'Mango',
        precio: 1.23,
        imagen: 'item8.jpeg'
    }

];

let cartItems = [];

function renderProducts(){


    for( let i = 0; i < productos.length; i++){

        console.log("Hola")

        let divElementItem = elementoBasico("<div>", "item")
        let imgElementItem = elementoBasico("<img>", "item-img")
        let url = './assets/' + productos[i].imagen;
        imgElementItem.attr({class: "item-img", src :url});

        //VARIABLES
        let divElementItemDescription = $("<div>");
        let nameElementItem = $("<p>");
        let divElementItemOrder = $("<div>");
        let priceElementItem = $("<h3>");
        let buttonElementItem = $("<button>");

        //ATRIBUTOS
        divElementItemDescription.attr({class : "item-description"});
        nameElementItem.attr({class: "item-name"});
        divElementItemOrder.attr({class : "item-order"});
        priceElementItem.attr({class: "item-price"});
        buttonElementItem.attr({class:"item-add"});
        buttonElementItem.click(function() { addProductsToCart(productos[i]) });
        nameElementItem.text(productos[i].nombre);
;

        //TEXTO
        priceElementItem.text(productos[i].precio + '€');
        buttonElementItem.text("Añadir al carrito");
        

        //RESULTADO POR PANTALLA
        divElementItemDescription.append(imgElementItem);
        divElementItemDescription.append(nameElementItem);

        divElementItemOrder.append(priceElementItem);
        divElementItemOrder.append(buttonElementItem);

        divElementItem.append(divElementItemDescription);
        divElementItem.append(divElementItemOrder);

        $("#listItems").append(divElementItem);
    }
}

function addProductsToCart(item){
    cartItems.push(item);
    renderCart();
}

function renderCart(){

    $("#cartItem").text(""); /* Truki para eliminar contenido dentro del div */

    let totalPrice = 0;
    //comprobamos el estado del carrito, si no tiene items pintamos el dibujo de carrito vacío
    if(cartItems.length > 0){
        //eliminaremos la información de que no hay nada en el carro
        $("#cartNoItems").hide();

        //pintaremos la lista
        for( let i = 0; i < cartItems.length; i++){

            //VARIABLES
            let elementCartItem = $("<div>");
            let elementCartImgItem = $("<img>");
            let elementCartDescription = $("<p>");
            let elementCartPrice = $("<p>");
            let elementCartQuantity = $("<p>");
            let elementItemBtn = $("<button>");


            // ATRIBUTOS
            elementCartItem.attr({class : "cart-item"});
            let url = './assets/' + cartItems[i].imagen;
            elementCartImgItem.attr({class: "cart-item-img", src :url});
            elementCartDescription.attr({class: "cart-item-description"});
            elementCartPrice.attr({class:"cart-item-price"});
            elementCartQuantity.attr({class: "cart-item-quantity"});
            elementItemBtn.attr({class:"cart-item-btn"});
            elementItemBtn.click(function() { deleteProductFromCart(i) });

            //TEXTO
            elementCartDescription.text(cartItems[i].nombre);
            elementCartPrice.text(cartItems[i].precio + '€');
            elementCartQuantity.text("1 Ud.");
            elementItemBtn.text('-');

            //RESULTADO POR PANTALLA

            elementCartItem.append(elementCartImgItem);
            elementCartItem.append(elementCartDescription);
            elementCartItem.append(elementCartPrice);
            elementCartItem.append(elementCartQuantity);
            elementCartItem.append(elementItemBtn);

            $("#cartItem").append(elementCartItem);
            console.log(cartItems[i].precio);
            totalPrice += cartItems[i].precio;
            console.log(totalPrice) 
            

        }
        let elementTotal = $("<p>");
        elementTotal.attr({class: "cart-total"});
        elementTotal.text("Total: " + totalPrice + "€");
        $("#cartItem").append(elementTotal);

    } else {
        $("#cartNoItems").show();
    }
}

function deleteProductFromCart(elementPosition){
    let cartItemsAux = [];

    for(let i = 0; i< cartItems.length; i++){
        if(i !== elementPosition){
            cartItemsAux.push(cartItems[i]);
        }
    }
    cartItems = cartItemsAux;
    renderCart();
}


function elementoBasico(tipoElemento, clase, texto, img){
    
    let elemento = $(tipoElemento)
    elemento.attr({class: clase, src: img})
    elemento.text(texto)


    return elemento;
}