var disponible = true;

//FUNCIONES HTML

window.onload = function(){
    $(document).ready(bloqueHeader);
    $(document).ready(bloqueSection);
    $(document).ready(renderProducts);
    $(document).ready(renderCart);
    $(document).ready(bloqueFooter);
}

function bloqueHeader(){
    //VARIABLES
    let header = $("<header>");
    let imgHeader = $("<img>");

    //ATRIBUTOS

    header.attr({class : "header"});
    imgHeader.attr({class : "logo", src: "assets/logo.png"});

    //RESULTADO POR PANTALLA
    $("body").append(header);
    $(header).append(imgHeader);
}
function bloqueFooter(){
    //VARIABLES
    let footer = $("<footer>");
    let divFooter = $("<div>");
    let divLinksFooter = $("<div>");
    let divFooterColumn1 = $("<div>");
    let divFooterColumn2 = $("<div>");
    let divFooterColumn3 = $("<div>");
    let divFooterColumn4 = $("<div>");
    let imgColumn1Footer1 = $("<img>");
    let aColumn2Footer1 = $("<a>");
    let aColumn2Footer2 = $("<a>");
    let aColumn2Footer3 = $("<a>");
    let aColumn3Footer1 = $("<a>");
    let aColumn3Footer2 = $("<a>");
    let aColumn3Footer3 = $("<a>");
    let aColumn4Footer1 = $("<a>");
    let aColumn4Footer2 = $("<a>");
    let aColumn4Footer3 = $("<a>");



    //ATRIBUTOS
    footer.attr({class : "footer"});
    divFooter.attr({class : "footer-container"});
    divLinksFooter.attr({class : "footer-links"});
    divFooterColumn1.attr({class : "footer-link-column"});
    divFooterColumn2.attr({class : "footer-link-column"});
    divFooterColumn3.attr({class : "footer-link-column"});
    divFooterColumn4.attr({class : "footer-link-column"});
    imgColumn1Footer1.attr({src: "assets/logo-footer.png", class:"img-link"});
    aColumn2Footer1.attr({class : "footer-link", href:"#"});
    aColumn2Footer2.attr({class : "footer-link", href:"#"});
    aColumn2Footer3.attr({class : "footer-link", href:"#"});
    aColumn3Footer1.attr({class : "footer-link", href:"#"});
    aColumn3Footer2.attr({class : "footer-link", href:"#"});
    aColumn3Footer3.attr({class : "footer-link", href:"#"});
    aColumn4Footer1.attr({class : "footer-link", href:"#"});
    aColumn4Footer2.attr({class : "footer-link", href:"#"});
    aColumn4Footer3.attr({class : "footer-link", href:"#"});

    //TEXTO
    aColumn2Footer1.text("Trabaja con nosotros");
    aColumn2Footer2.text("Acerca de nosotros");
    aColumn2Footer3.text("Preguntas frecuentes");
    aColumn3Footer1.text("Blog");
    aColumn3Footer2.text("Contactos");
    aColumn3Footer3.text("Seguridad");
    aColumn4Footer1.text("Facebook");
    aColumn4Footer2.text("Twitter");
    aColumn4Footer3.text("Instagram");

    //RESULTADO POR PANTALLA

    $("body").append(footer);
    $(footer).append(divFooter);
    $(divFooter).append(divLinksFooter);

    $(divLinksFooter).append(divFooterColumn1);
    $(divFooterColumn1).append(imgColumn1Footer1);

    $(divLinksFooter).append(divFooterColumn2);
    $(divFooterColumn2).append(aColumn2Footer1);
    $(divFooterColumn2).append(aColumn2Footer2);
    $(divFooterColumn2).append(aColumn2Footer3);

    $(divLinksFooter).append(divFooterColumn3);
    $(divFooterColumn3).append(aColumn3Footer1);
    $(divFooterColumn3).append(aColumn3Footer2);
    $(divFooterColumn3).append(aColumn3Footer3);

    $(divLinksFooter).append(divFooterColumn4);
    $(divFooterColumn4).append(aColumn4Footer1);
    $(divFooterColumn4).append(aColumn4Footer2);
    $(divFooterColumn4).append(aColumn4Footer3);

}

