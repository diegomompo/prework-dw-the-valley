var section = $("<section>");

let divItems = $("<div>");
divItems.attr({id: "listItems", class: "supermarket-items"});

let divNoProducts = $("<div>");
let divProducts = $("<div>");

divNoProducts.attr({id:"cartNoItems", class:"cart-no-products"});
divProducts.attr({id:"cartItem" , class:"cart-products"});

function bloqueSection(){
    //FUNCIONES HTML
    bloqueSectionMain();
    bloqueSectionAside();

    $("body").append(section);
}

function bloqueSectionMain(){
    //VARIABLE
    let mainSection = $("<main>");
    let h2Main = $("<h2>");


    //ATRIBUTOS
    mainSection.attr({class: "main"});
    h2Main.attr({class: "main-text"});

    //TEXTO
    h2Main.text("Frutas");


    //RESULTADO POR PANTALLA
    $(section).append(mainSection);
    $(mainSection).append(h2Main);
    $(mainSection).append(divItems)
}

function bloqueSectionAside(){
    //VARIABLE
    let asideSection = $("<aside>");
    let h2Aside = $("<h2>");
    let pNoProducts = $("<p>");

    //ATRIBUTOS
    asideSection.attr({class: "cart"});
    h2Aside.attr({class: "cart-text"});

    pNoProducts.attr({class: "no-product-text"});

    //TEXTO

    h2Aside.text("Carro");
    pNoProducts.text("Todavía no has añadido ningún producto. Cuando lo hagas, ¡verás los productos aquí!");

    //RESULTADO POR PANTALLA

    $(section).append(asideSection);
    $(asideSection).append(h2Aside);

    $(asideSection).append(divNoProducts);
    $(divNoProducts).append(pNoProducts);
    $(asideSection).append(divProducts);

}