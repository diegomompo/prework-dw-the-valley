var disponible = true

//FUNCIONES HTML

window.onload = function () {
    $(document).ready(bloqueIntro)
    $(document).ready(bloqueValueProposal)
    $(document).ready(bloqueBanner)
    $(document).ready(bloqueFAQ)
    $(document).ready(bloqueFooter)

}
//BLOQUE INTRODUCCIÓN
function bloqueIntro() {

    //VARIBALES DEL BLOQUE
    var headerIntro = $("<header>")
    var imgIntro = $("<img>")
    var buttonLoginIntro = $("<button>")
    var sectionIntro = $("<section>")
    var h1Intro = $("<h1>")
    var h2Intro = $("<h2>")
    var pIntro = $("<p>")
    var buttonCompraIntro = $("<button>")

    //ATRIBUTOS BLOQUE
    sectionIntro.attr({ class: "intro" })
    imgIntro.attr({ src: "assets/logo.png", class: "logo" })
    buttonLoginIntro.attr({ class: "buttonIntro login-button" })
    h1Intro.attr({ class: "intro-title" })
    h2Intro.attr({ class: "intro-subtitle" })
    pIntro.attr({ class: "intro-parrafo" })
    buttonCompraIntro.attr({ class: "buttonIntro compra-button" })

    //TEXTOS DEL BLOQUE
    buttonLoginIntro.text("SUSCRÍBETE YA")
    h1Intro.text("DI HOLA A TODO LO QUE AMAS")
    h2Intro.text("8,99 €/mes")
    pIntro.text("Cancela en cualquier momento")
    buttonCompraIntro.text("AHORRA YA")

    //MOSTRAR EN PANTALLA
    $("body").append(sectionIntro)
    $(sectionIntro).append(headerIntro)
    $(headerIntro).append(imgIntro)
    $(headerIntro).append(buttonLoginIntro)
    $(sectionIntro).append(h1Intro)
    $(sectionIntro).append(h2Intro)
    $(sectionIntro).append(pIntro)
    $(sectionIntro).append(buttonCompraIntro)

}

//BLOQUE VALUE PROPOSAL
function bloqueValueProposal() {

    //VARIBALES DEL BLOQUE
    var sectionProposal = $("<section>")
    var h3Proposal = $("<h3>")
    var pProposal = $("<p>")

    //ATRIBUTOS BLOQUE
    sectionProposal.attr({ class: "proposal" })
    h3Proposal.attr({ class: "proposal-text" })
    pProposal.attr({ class: "proposal-parrafo" })

    //TEXTOS DEL BLOQUE
    h3Proposal.text("Todo lo que amas en un mismo lugar")
    pProposal.text("Los mayores éxitos de la taquilla, las historias más rompedoras y los clásicos inolvidables que nos han convertido en quienes somos.")

    //MOSTRAR EN PANTALLA
    $("body").append(sectionProposal)
    $(sectionProposal).append(h3Proposal)
    $(sectionProposal).append(pProposal)
}

function bloqueBanner() {

    //VARIBALES DEL BLOQUE
    var sectionBanner1 = $("<section>")
    var divBanner1 = $("<div>")
    var imgBanner1 = $("<img>")
    var imgTitleBanner1 = $("<img>")
    var h3Banner1 = $("<h3>")
    var h3Banner11 = $("<h3>")
    var buttonBanner1 = $("<button>")
    var sectionBanner2 = $("<section>")
    var divBanner2 = $("<div>")
    var imgBanner2 = $("<img>")
    var imgTitleBanner2 = $("<img>")
    var h3Banner2 = $("<h3>")
    var h3Banner21 = $("<h3>")
    var buttonBanner2 = $("<button>")


    //ATRIBUTOS BLOQUE
    sectionBanner1.attr({ class: "banner banner1" })
    divBanner1.attr({class: "banner-info"})
    imgBanner1.attr({ src: "assets/banner-1.jpg", class: "banner-image1" })
    imgTitleBanner1.attr({ src: "assets/banner-1-logo.png", class: "banner-imageLogo1" })
    h3Banner1.attr({ class: "textBanner text1" })
    h3Banner11.attr({ class: "textBanner text1" })
    buttonBanner1.attr({ class: "buttonBanner button1" })

    sectionBanner2.attr({ class: "banner banner2" })
    divBanner2.attr({class: "banner-info banner2"})
    imgBanner2.attr({ src: "assets/banner-2.png", class: "banner-image2" })
    imgTitleBanner2.attr({ src: "assets/banner-2-logo.png", class:  "banner-imageLogo2" })
    h3Banner2.attr({ class: "textBanner text2" })
    h3Banner21.attr({ class: "textBanner text2" })
    buttonBanner2.attr({ class: "buttonBanner button2" })

    //TEXTOS DEL BLOQUE
    h3Banner1.text("YA DISPONIBLE EN HBO MAX.")
    h3Banner11.text("EXCLUSIVAMENTE PARA SUSCRIPTORES")
    buttonBanner1.text("VER TRAILER")
    h3Banner2.text("YA DISPONIBLE EN HBO MAX.")
    h3Banner21.text("EXCLUSIVAMENTE PARA SUSCRIPTORES")
    buttonBanner2.text("VER TRAILER")

    //MOSTRAR EN PANTALLA
    $("body").append(sectionBanner1)
    $(sectionBanner1).append(divBanner1)
    $(divBanner1).append(imgBanner1)
    $(divBanner1).append(imgTitleBanner1)
    $(divBanner1).append(h3Banner1)
    $(divBanner1).append(h3Banner11)
    $(divBanner1).append(buttonBanner1)

    $("body").append(sectionBanner2)
    $(sectionBanner2).append(divBanner2)
    $(divBanner2).append(imgBanner2)
    $(divBanner2).append(imgTitleBanner2)
    $(divBanner2).append(h3Banner2)
    $(divBanner2).append(h3Banner21)
    $(divBanner2).append(buttonBanner2)

}
function bloqueFAQ() {

    //VARIBALES DEL BLOQUE
    var sectionFAQ = $("<section>")
    var h3FAQ = $("<h3>")
    var detailsFAQ1 = $("<details>")
    var summaryFAQ1 = $("<summary>")
    var pFAQ1 = $("<p>")
    var detailsFAQ2 = $("<details>")
    var summaryFAQ2 = $("<summary>")
    var pFAQ2 = $("<p>")
    var detailsFAQ3 = $("<details>")
    var summaryFAQ3 = $("<summary>")
    var pFAQ3 = $("<p>")
    var detailsFAQ4 = $("<details>")
    var summaryFAQ4 = $("<summary>")
    var pFAQ4 = $("<p>")


    //ATRIBUTOS BLOQUE
    sectionFAQ.attr({ class: "faqs"})
    h3FAQ.attr({class: "faq-title"})
    detailsFAQ1.attr({class: "faq"})
    summaryFAQ1.attr({class: "summary"})
    detailsFAQ2.attr({class: "faq"})
    summaryFAQ2.attr({class: "summary"})
    detailsFAQ3.attr({class: "faq"})
    summaryFAQ3.attr({class: "summary"})
    detailsFAQ4.attr({class: "faq"})
    summaryFAQ4.attr({class: "summary"})



    //TEXTOS DEL BLOQUE
    h3FAQ.text("Lo que todo el mundo se pregunta sobre HBO Max")
    summaryFAQ1.text("¿Qué es HBO Max?")
    pFAQ1.text("HBO Max es un servicio de streaming nuevo que te trae las mejores historias de Warner Bros., HBO, DC, Cartoon Network y mucho más, juntos por primera vez.")
    summaryFAQ2.text("¿Cómo puedo recibir ayuda si tengo algún problema?")
    pFAQ2.text("Si tienes algún problema al suscribirte o al acceder a HBO Max, encontrarás ayuda en nuestro Centro de Ayuda. ¡Estarás viendo HBO Max antes de que las palomitas estén listas!")
    summaryFAQ3.text("¿Cuánto cuesta HBO Max?")
    pFAQ3.text("HBO Max tiene un precio mensual de solo 8,99 € y puedes conseguir un 35 % de descuento con la suscripción anual de 69,99 €. ¡Son cuatro meses gratis al año!")
    summaryFAQ4.text("¿Puedo modificar o cancelar mi suscripción?")
    pFAQ4.text("¡Sí! Puedes pasar de la suscripción mensual a la anual y viceversa. También puedes cancelar tu suscripción en cualquier momento.")
  
    //MOSTRAR EN PANTALLA
    $("body").append(sectionFAQ)
    $(sectionFAQ).append(h3FAQ)

    $(sectionFAQ).append(detailsFAQ1)
    $(detailsFAQ1).append(summaryFAQ1)
    $(detailsFAQ1).append(pFAQ1)

    $(sectionFAQ).append(detailsFAQ2)
    $(detailsFAQ2).append(summaryFAQ2)
    $(detailsFAQ2).append(pFAQ2)

    $(sectionFAQ).append(detailsFAQ3)
    $(detailsFAQ3).append(summaryFAQ3)
    $(detailsFAQ3).append(pFAQ3)

    $(sectionFAQ).append(detailsFAQ4)
    $(detailsFAQ4).append(summaryFAQ4)
    $(detailsFAQ4).append(pFAQ4)

}

function bloqueFooter(){
    
    //VARIBALES DEL BLOQUE

    var footer = $("<footer>")
    var divFotter = $("<div>")
    var divColumnFooters1 = $("<div>")
    var aColumnFooters1 = $("<a>")
    var divColumnFooters2 = $("<div>")
    var aColumnFooters2 = $("<a>")
    var divColumnFooters3 = $("<div>")
    var aColumnFooters3 = $("<a>")
    var divColumnFooters4 = $("<div>")
    var aColumnFooters4 = $("<a>")
    var divColumnFooters5 = $("<div>")
    var aColumnFooters5 = $("<a>")
    var pFooter = $("<p>")

    //ATRIBUTOS BLOQUE

    footer.attr({class : "footer"})
    divFotter.attr({class: "footer-links"})
    divColumnFooters1.attr({class: "footer-links-column", href:"#"})
    aColumnFooters1.attr({class: "footer-link", href:"#"})
    divColumnFooters2.attr({class: "footer-links-column", href:"#"})
    aColumnFooters2.attr({class: "footer-link", href:"#"})
    divColumnFooters3.attr({class: "footer-links-column", href:"#"})
    aColumnFooters3.attr({class: "footer-link", href:"#"})
    divColumnFooters4.attr({class: "footer-links-column", href:"#"})
    aColumnFooters4.attr({class: "footer-link", href:"#"})
    divColumnFooters5.attr({class: "footer-links-column", href:"#"})
    aColumnFooters5.attr({class: "footer-link", href:"#"})
    pFooter.attr({class: "footer-text"})

    //TEXTOS DEL BLOQUE

    aColumnFooters1.text("POLITICA DE PRIVACIDAD")
    aColumnFooters2.text("GESTIONAR PREFERENCIAS")
    aColumnFooters3.text("CONDICIONES DE USO")
    aColumnFooters4.text("CENTRO DE AYUDA")
    aColumnFooters5.text("INFORMACIÓN CORPORATIVA")
    pFooter.text("©2022 HBO Nordic AB. All Rights Reserved. HBO MAX™ is used under license")

    //MOSTRAR EN PANTALLA

    $("body").append(footer)
    $(footer).append(divFotter)

    $(divFotter).append(divColumnFooters1)
    $(divColumnFooters1).append(aColumnFooters1)

    $(divFotter).append(divColumnFooters2)
    $(divColumnFooters2).append(aColumnFooters2)

    $(divFotter).append(divColumnFooters3)
    $(divColumnFooters3).append(aColumnFooters3)

    $(divFotter).append(divColumnFooters4)
    $(divColumnFooters4).append(aColumnFooters4)

    $(divFotter).append(divColumnFooters5)
    $(divColumnFooters5).append(aColumnFooters5)

    $(footer).append(pFooter)
}