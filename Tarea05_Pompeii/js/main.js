let products = [
    {
        id: 1,
        name: 'HIGBY CARAMEL',
        price: 79,
        urlImg: './assets/higby.png'
    },
    {
        id: 2,
        name: 'ELAN OFF WHITE',
        price: 109,
        urlImg: './assets/elan.png'
    },
    {
        id: 3,
        name: 'DELTA MARINE',
        price: 85,
        urlImg: './assets/delta.png'
    },
    {
        id: 4,
        name: 'CATALINA NAVI',
        price: 85,
        urlImg: './assets/catalina.png'
    },
    {
        id: 5,
        name: 'MISTRAL ALOE',
        price: 109,
        urlImg: './assets/mistral.png'
    }

];

$(document).ready(function () {

    renderProductList()
    renderProduct(products[0])


});


function renderProductList() {
    let elementProductlist = $(".product-list")
    console.log(products[0].name)
    for (let i = 0; i < products.length; i++) {
        console.log(products[0].name)
        let elementButton = $("<button>")
        elementButton.text(products[i].name)
        elementButton.attr({ class: "btn" })
        elementButton.click(function () { renderProduct(products[i]) })
        $(elementProductlist).append(elementButton)
    }

}

function renderProduct(item){

    let elementProduct = $(".product")

    elementProduct.text("")
    let elementImg = $("<img>")
    elementImg.attr({src: item.urlImg, class : "img-product"})

    let elementPrice = $("<p>");
    elementPrice.text(item.price + "€");
    elementPrice.attr({class: "price-product"});

    let elementBtn = $("<button>");
    elementBtn.text("COMPRAR");
    elementBtn.attr({class: "btn-product"});
    elementBtn.click(purchase)

    $(elementProduct).append(elementImg);
    $(elementProduct).append(elementPrice);
    $(elementProduct).append(elementBtn);

    function purchase (){
        let elementProduct = $(".product")

        elementProduct.text("")
        let elementPurchase = $("<p>");
        elementPurchase.text("GRACIAS POR TU COMPRA");
    
        $(elementProduct).append(elementPurchase);
    }

}
